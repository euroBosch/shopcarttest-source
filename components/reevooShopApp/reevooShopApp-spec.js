describe('reevooShopApp', function() {

    beforeEach(module('ReevooShop'));

    var scope, component, $componentController;

    beforeEach(inject(function($rootScope, _$componentController_) {
        scope = $rootScope.$new();
        $componentController = _$componentController_;
        component = $componentController('reevooShopApp', { $scope: scope });
    }));


    it('should run a first test', function() {

        var test = "RADU";
        expect(component.testRet(test)).toBe(test);

        /* 
        To test your directive, you need to create some html that would use your directive,
        send that through compile() then compare the results.

        var element = compile('<div mydirective name="name">hi</div>')(scope);
        expect(element.text()).toBe('hello, world');
        */

    });
});
