function reevooAppController(){
	this.testRet = function(arg){ //just testing the test
		return arg;
	};
}

angular.module('ReevooShop')
    .component('reevooShopApp', {
        templateUrl: 'components/reevooShopApp/reevooShopApp.html',
        $routeConfig: [
            { path: '/shop/...', name: 'Shop', component: 'frontEnd',useAsDefault: true },
            { path: '/about', name: 'About', component: 'about'},
        ],
        controller: reevooAppController
    });