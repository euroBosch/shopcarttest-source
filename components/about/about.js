function aboutPageController() {

}

angular.module('ReevooShop')
    .component('about', {
    	bindings: {
            item: "=",
            itemIndex: "="
        },
        templateUrl: 'components/about/about.html',
        controller: aboutPageController
    });

