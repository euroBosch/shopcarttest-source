(function() {
    function appConfig($locationProvider, $compileProvider, ShoppingCartServiceProvider, DISCOUNT_CONFIG) {

        $compileProvider.debugInfoEnabled(false); //remove debugging info (better speed, dom)

        // HTML 5 mode
        $locationProvider
            .html5Mode(true)
            .hashPrefix('!');

        //applying discount scheme
        // probably loaded from server
        ShoppingCartServiceProvider.setDiscountScheme(DISCOUNT_CONFIG);
    }

    function appRun($rootScope) {
        //not used yet but...you never know when it gets handy
        $rootScope.safeApply = function(fn) {
            var phase = $rootScope.$$phase;
            if (phase === '$apply' || phase === '$digest') {
                if (fn && (typeof(fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };
    }

    angular.module('ReevooShop', ['ngComponentRouter', 'ngAnimate', 'shop'])
        .value('$routerRootComponent', 'reevooShopApp')
        .config(appConfig)
        .run(appRun);
}());
