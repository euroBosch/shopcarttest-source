(function() {
    function shoppingCartController(ShoppingCartService) {
        ShoppingCartService.getItems()
            .then(data => this.items = data)
            .catch(err => console.warn(err));

        this.shopCartSumary = ShoppingCartService.getCartSummary();

        this.emptyCart = function() {
            console.warn("TODO: aici as pune o modala de confirmare ");
            ShoppingCartService.emptyCart();
        };
    }

    angular.module('shop')
        .component('shoppingCart', {
            bindings: {
                someBindings: "="
            },
            templateUrl: 'shop/components/shoppingCart/shoppingCart.html',
            controller: shoppingCartController
        });
}());
