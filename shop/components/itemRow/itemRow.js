(function() {
    function itemRowController(RestAPIService, ShoppingCartService) {
        // console.info("itemRowController");
        let $ctrl = this;

        $ctrl.addInProgress = false;
        $ctrl.errorOnRemoval = {};

        function _addSuccess(data) {
            $ctrl.addInProgress = false;
        }

        function _addError(err) {
            // console.warn("ERROR from service:" + err.message); //should popup an error mesage to the user instead
            $ctrl.addInProgress = false;
            $ctrl.errorOnRemoval.code = err.code;
            $ctrl.errorOnRemoval.message = err.message; //we might use this to show the error message
        }

        this.addToCart = function() {
            $ctrl.addInProgress = true;
            ShoppingCartService.addToCart(this.item)
                .then(_addSuccess)
                .catch(_addError);
        };
    }

    angular.module('shop')
        .component('itemRow', {
            bindings: {
                item: "=",
                itemIndex: "="
            },
            templateUrl: 'shop/components/itemRow/itemRow.html',
            controller: itemRowController
        });
}());
