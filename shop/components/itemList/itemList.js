(function() {
	function itemListController(RestAPIService, ShoppingCartService) {
    RestAPIService.getItems()
        .then(data=>this.items = data)
        .catch(err=>console.warn(err));
}

angular.module('shop')
    .component('itemList', {
        templateUrl: 'shop/components/itemList/itemList.html',
        controller: itemListController
    });
}());