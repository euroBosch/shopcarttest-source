function frontendController(){

	this.testRet = function(arg){
		return arg;

	};
}

angular.module('shop')
    .component('frontEnd', {
        templateUrl: 'shop/components/frontEnd/frontEnd.html',
        $routeConfig: [
            { path: '/', name: 'FrontEnd', component: 'itemList',useAsDefault: true },
        ],
        controller: frontendController
    });
