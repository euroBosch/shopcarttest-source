describe('shoppingCartItem', function() {
    let scope, component, $componentController, shopcartServiceMock;
    let $q, deferred;
    let $scope;

    beforeEach(function() {
        module("shop");

        module(function($provide) {
            $provide.service('ShoppingCartService', ["$q", function($q) {
                this.shouldPass = true;

                this.removeFromCart = function(product) {
                    if (this.shouldPass) {
                        return $q.when();
                    } else {
                        return $q.reject();
                    }
                };
            }]);

        });
    });

    beforeEach(inject(function(_$rootScope_, _$q_, _$componentController_, ShoppingCartService) {
        $q = _$q_;
        $scope = _$rootScope_.$new(); //needed only for $apply, $scope is kinda deprecated nowadays (except for events or watchers)
        deferred = _$q_.defer();
        $componentController = _$componentController_;
        shopcartServiceMock = ShoppingCartService;
        component = $componentController('shoppingCartItem');
    }));


    it('should call to remove an element from the shoppingCart', function() {

        spyOn(shopcartServiceMock, 'removeFromCart').and.callThrough(); //.andReturn(fakeHttpPromise);

        component.item = { test: "Radu" };
        component.removeFromCart();
        expect(shopcartServiceMock.removeFromCart).toHaveBeenCalledWith({ test: "Radu" });
    });

    it('should do something if service returns an errored promise', function() {
        spyOn(shopcartServiceMock, 'removeFromCart').and.returnValue(deferred.promise);

        component.item = { test: "Radu" };
        shopcartServiceMock.shouldPass = false;
        component.removeFromCart();
        expect(shopcartServiceMock.removeFromCart).toHaveBeenCalledWith({ test: "Radu" });

        let err = { code: 101, message: "Mock error" };
        deferred.reject(err);
        $scope.$apply(); //sorry but we need this
        expect(component.errorOnRemoval).toEqual(err);//but actually we're not doing anything in the controller to process the error
    });

    it('should know we are waiting for the product to be removed', function() {

        spyOn(shopcartServiceMock, 'removeFromCart').and.returnValue(deferred.promise);

        component.item = { test: "Radu" };
        expect(component.removeInProgress).toBe(false);
        component.removeFromCart();
        expect(component.removeInProgress).toBe(true);
        expect(shopcartServiceMock.removeFromCart).toHaveBeenCalledWith({ test: "Radu" });
        
        deferred.resolve({ name: "blah" });
        $scope.$apply(); //sorry but we need this
        expect(component.removeInProgress).toBe(false); //the removal has been performed 
        expect(component.errorOnRemoval).toEqual({});//ensure we didn't have an error
    });

});
