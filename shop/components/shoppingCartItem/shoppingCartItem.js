(function() {
    function shoppingCartItemController(ShoppingCartService) {
        let $ctrl = this;

        $ctrl.removeInProgress = false;
        $ctrl.errorOnRemoval = {};

        function _removeSuccess(data) {
            $ctrl.removeInProgress = false;
        }

        function _removeError(err) {
            // console.warn("ERROR from service:" + err.message); //should popup an error mesage to the user instead
            $ctrl.removeInProgress = false;
            $ctrl.errorOnRemoval.code = err.code;
            $ctrl.errorOnRemoval.message = err.message; //we might use this to show the error message
        }


        $ctrl.removeFromCart = function() {
            $ctrl.removeInProgress = true;

            ShoppingCartService.removeFromCart($ctrl.item)
                .then(_removeSuccess)
                .catch(_removeError);
        };

    }

    angular.module('shop')
        .component('shoppingCartItem', {
            bindings: {
                item: "=",
                itemIndex: "="
            },
            templateUrl: 'shop/components/shoppingCartItem/shoppingCartItem.html',
            controller: shoppingCartItemController
        });
}());
