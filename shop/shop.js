(function() {
    function appConfig($locationProvider, $compileProvider) {

        $compileProvider.debugInfoEnabled(false); //remove debugging info (better speed, dom)

        // HTML 5 mode
        $locationProvider
            .html5Mode(true)
            .hashPrefix('!');
    }

    function appRun($rootScope) {

        $rootScope.safeApply = function(fn) {
            var phase = $rootScope.$$phase;
            if (phase === '$apply' || phase === '$digest') {
                if (fn && (typeof(fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };
    }


    angular.module('shop', ['ngComponentRouter', 'shop'])
        .value('$routerRootComponent', 'frontEnd')
        .config(appConfig)
        .run(appRun);
}());
