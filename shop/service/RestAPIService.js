class Product {
    constructor(code, name, price, img) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.img = img;
    }

    get() {
        return this;
    }
}


class RestAPIService {


    constructor($q) {
        this._$q = $q;

        this._items = [];
        this._items.push(new Product("FR1", "Fruit tea", 3.11, "assets/images/fruitTea.jpg"));
        this._items.push(new Product("SR1", "Strawberries", 5.00, "assets/images/strawberries.jpg"));
        this._items.push(new Product("CF1", "Coffee", 11.23, "assets/images/coffee.jpg"));
    }

    getItems() {
        return this._$q.when(this._items);
    }

}


angular.module('shop').service('RestAPIService', RestAPIService);
