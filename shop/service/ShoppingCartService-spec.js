describe('ShoppingCartService WITHOUT a discount scheme', function() {
    let shopCartService;
    let $timeout;
    let PRODUCT_MOCKS = [
        { code: "FR1", name: "Fruit tea", price: 3.11, img: "assets/images/fruitTea.jpg" },
        { code: "SR1", name: "Strawberries", price: 5.00, img: "assets/images/strawberries.jpg" },
        { code: "CF1", name: "Coffee", price: 11.23, img: "assets/images/coffee.jpg" }
    ];

    beforeEach(module('shop'));

    beforeEach(inject(function(_$timeout_, _ShoppingCartService_) {
        shopCartService = _ShoppingCartService_;
        $timeout = _$timeout_;
    }));

    describe("Basics", function() {
        it('should exist :)', function() {
            expect(shopCartService).toBeDefined();
        });

        it('should have an empty list when constructed', function(done) {
            shopCartService.getItems()
                .then(data => {
                    expect(data).toBeDefined();
                    expect(data.length).toBe(0); //no data yet
                    done();
                })
                .catch(err => expect(err).toBeDefined()); //never happens, we don't have all errors for this test project. Later on we will not test for .catch()
            $timeout.flush();
        });
    });

    describe("when Adding", function() {
        let product = PRODUCT_MOCKS[0];

        it('should add an item', function(done) {
            shopCartService.addToCart(product)
                .then(data => {
                    expect(data).toBeDefined();
                    expect(data).toBe(product);
                    done();
                })
                .catch(err => expect(err).toBeDefined());
            $timeout.flush();
        });

        it('should only contain the item added to the empty list', function(done) {
            shopCartService.addToCart(product);

            shopCartService.getItems()
                .then(data => {
                    expect(data.length).toBe(1);
                    expect(data[0]).toBe(product);
                    done();
                });
            $timeout.flush();
        });

        it('should compute the shop total price after adding one item', function(done) {
            shopCartService.addToCart(product).then(() => {
                expect(shopCartService.getCartSummary().sumOfItems).toBe(product.price);
                done();
            });
            $timeout.flush();
        });

        it('should have no discount applied', function(done) {
            shopCartService.addToCart(product);
            shopCartService.addToCart(product).then(() => {
                expect(shopCartService.getCartSummary().discount).toBe(0);
                done();
            });
            $timeout.flush();
        });

        it('should add 3 different items and have a count of 3 items and the correct sum', function(done) {
            let product0 = PRODUCT_MOCKS[0],
                product1 = PRODUCT_MOCKS[1],
                product2 = PRODUCT_MOCKS[2],
                expectedSum = product0.price + product1.price + product2.price;

            shopCartService.addToCart(product0);
            shopCartService.addToCart(product1);
            shopCartService.addToCart(product2);

            shopCartService.getItems()
                .then(data => {
                    expect(data.length).toBe(3);
                    expect(shopCartService.getCartSummary().sumOfItems).toBe(expectedSum);
                    done();
                });

            $timeout.flush();
        });

        it('should add 2 items of the second product', function(done) {
            let expectedSum = product.price * 2;

            shopCartService.addToCart(product);
            shopCartService.addToCart(product);
            shopCartService.getItems()
                .then(data => {
                    expect(data.length).toBe(1); //1 kind of item in the cart
                    expect(data[0].code).toBe(product.code);
                    expect(data[0].count).toBe(2); //2 of the same item
                    let cartSummary = shopCartService.getCartSummary();
                    expect(cartSummary.sumOfItems).toBe(expectedSum);
                    expect(cartSummary.discount).toBe(0);
                    done();
                });

            $timeout.flush();
        });
    });

    describe("when Removing", function() {
        let product = PRODUCT_MOCKS[0];

        xit('should remove an item (DAMN nested callback, be back later)', function(done) {

            shopCartService.addToCart(product)
                .then(() => {
                	console.log("ADDIIIIIING");
                    // $timeout.flush();
                    shopCartService.removeFromCart(product) //nasty but let's explore more ways of doing it
                        .then(data => {
                            // $timeout.flush();
                            console.log("REMOVING");
                            expect(data).toBe(product);
                            shopCartService.getItems() //asure we have an empty cart
                                .then(data => {
                                    $timeout.flush();
                                    console.log("GET ITEEEEEMS");
                                    expect(data.length).toBe(1);
                                    done();
                                })
                                .finally(() => $timeout.flush());
                        })
                        .catch(err => expect(err).toBeDefined()); //should not execute (yet)
                    // .finally(()=>$timeout.flush());
// done();
                });
            // .finally(() => $timeout.flush());
            $timeout.flush();
        });

        xit('should only contain the item added to the empty list', function(done) {
            shopCartService.addToCart(product);

            shopCartService.getItems()
                .then(data => {
                    expect(data.length).toBe(1);
                    expect(data[0]).toBe(product);
                    done();
                });
            $timeout.flush();
        });

        xit('should compute the shop total price after adding one item', function(done) {
            shopCartService.addToCart(product).then(() => {
                expect(shopCartService.getCartSummary().sumOfItems).toBe(product.price);
                done();
            });
            $timeout.flush();
        });

        xit('should have no discount applied', function(done) {
            shopCartService.addToCart(product).then(() => {
                expect(shopCartService.getCartSummary().discount).toBe(0);
                done();
            });
            $timeout.flush();
        });

        xit('should add 3 different items and have a count of 3 items and the correct sum', function(done) {
            let product0 = PRODUCT_MOCKS[0],
                product1 = PRODUCT_MOCKS[1],
                product2 = PRODUCT_MOCKS[2],
                expectedSum = product0.price + product1.price + product2.price;

            shopCartService.addToCart(product0);
            shopCartService.addToCart(product1);
            shopCartService.addToCart(product2);

            shopCartService.getItems()
                .then(data => {
                    expect(data.length).toBe(3);
                    expect(shopCartService.getCartSummary().sumOfItems).toBe(expectedSum);
                    done();
                });

            $timeout.flush();
        });
    });
});
describe('ShoppingCartService WITH a discount Scheme applied', function() {
	it("should just work :)", function(){

	});
});