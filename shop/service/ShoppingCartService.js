(function() {
    class ShoppingCart {
        constructor($q, $timeout) {
            this._$q = $q; //you know...:)
            this._$timeout = $timeout; //fake some delay
            this._items = [];
            this._summary = { //shopCart totals
                sumOfItems: 0,
                discount: 0,
                toPay: 0
            };
            this._discountScheme = {};

            this._computeShopcartSummary = () => {
                let sumOfItems = this._items.length ? this._items.reduce((sumOfItems, product) => sumOfItems += product.count * product.price, 0) : 0,
                    discount = this._computeDiscount(),
                    toPay = sumOfItems - discount;


                this._summary.sumOfItems = sumOfItems; //3.11, huh? :)
                this._summary.discount = discount;
                this._summary.toPay = toPay;
            };

            this._computeDiscount = () => {
                let discount = 0;
                if (!this._discountScheme) {
                    console.warn("No discount scheme provided!"); //error message to the user instead of console.blah
                    discount = 0;
                } else {
                    let _ds = this._discountScheme;

                    //some rules below may have been defined in the DiscountScheme as well if we wanted
                    if (_ds.BUY_ONE_GET_ONE_FREE_PRODUCTS && //to please the CEO
                        this._items.length === 1 && //applies only if we have a single kind of product in the cart (could have configured that as well)
                        this._items[0].count > 1 && //and only if we have 2 or more products of the same type
                        _ds.BUY_ONE_GET_ONE_FREE_PRODUCTS.includes(this._items[0].code) //and the product is on promotion
                    ) {
                        //TODO: Logical Issue:
                        // should WE add the 2nd product or give it for free if the user adds it? I took the 2nd path
                        discount = this._items[0].price;
                    }
                    //check for volume discounts to please the COO
                    else if (_ds.VOLUME_DISCOUNT) { //assume it's array, well structured
                        _ds.VOLUME_DISCOUNT.forEach(promoProduct => {
                            discount = this._items
                                .filter(product => product.code === promoProduct.code && product.count >= promoProduct.minQty)
                                .reduce((total, luckyProduct) => total += luckyProduct.count * luckyProduct.price * promoProduct.discount, 0);
                        });
                    }
                }
                return discount;
            };
        }

        getItems() {
            var deferred = this._$q.defer();
            this._$timeout(() => { //network delay
                deferred.resolve(this._$q.when(this._items)); //might also be errored but assume it's not (we test for errors in other places, c'mon, it's just a test app)); 
            }, 100);
            return deferred.promise;
        }

        addToCart(product) {
            var deferred = this._$q.defer();

            this._$timeout(() => { //network delay again
                var existingProduct = this._items.find(item => item.code === product.code);
                if (!existingProduct) {
                    product.count = 1;
                    this._items.push(product);
                } else {
                    existingProduct.count++;
                }
                this._computeShopcartSummary();

                deferred.resolve(product); //assume we don't have any errors to .reject()

            }, 500);

            return deferred.promise;
        }

        removeFromCart(product) {
            var deferred = this._$q.defer();

            this._$timeout(() => { //you know, it takes a while over HTTP
                var existingProductIdx = this._items.findIndex(item => item.code === product.code),
                    itemToRemove = this._items[existingProductIdx];

                if (existingProductIdx === -1) {
                    console.warn("how did you get here?");
                    deferred.reject({ code: 0, message: "ShopCart is already empty" }); //this used to be an Error object from the Error class but I removed it
                } else {
                    if (this._items[existingProductIdx].count > 1) {
                        this._items[existingProductIdx].count--;
                        deferred.resolve(itemToRemove);
                    } else {
                        this._items.splice(existingProductIdx, 1);
                    }
                    this._computeShopcartSummary();
                }
            }, 500);

            return deferred.promise;
        }

        getCartSummary() {
            return this._summary;
        }

        setDiscountScheme(scheme) {
            //should test the scheme for validity here before enabling it: 
            //  BUY_ONE_GET_ONE_FREE_PRODUCTS, 
            //  VOLUME_DISCOUNT structure, etc
            this._discountScheme = scheme;
        }

        emptyCart() {
            this._items.length = 0;
            this._computeShopcartSummary();
        }
    }

    class ShoppingCartService {
        constructor() {
            this._discountScheme = {};

            this.setDiscountScheme = function(scheme) {
                this._discountScheme = scheme;
            };
        }

        $get($q, $timeout) {
            this._shoppingCart = new ShoppingCart($q, $timeout);
            this._shoppingCart.setDiscountScheme(this._discountScheme);
            return this._shoppingCart;
        }
    }

    angular.module('shop').provider('ShoppingCartService', ShoppingCartService);
}());
