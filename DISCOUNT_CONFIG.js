angular.module('ReevooShop')
    .constant("DISCOUNT_CONFIG", {
        BUY_ONE_GET_ONE_FREE_PRODUCTS: ['FR1'],
        VOLUME_DISCOUNT: [{ code: 'SR1', minQty: 3, discount: 0.1 }]
    });
